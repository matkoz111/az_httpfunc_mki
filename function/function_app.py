import logging
import azure.functions as func
import requests
import json

app = func.FunctionApp()


@app.route(route="MKIHttp", auth_level=func.AuthLevel.ANONYMOUS)
def MKIHttp(req: func.HttpRequest) -> func.HttpResponse:
    study_name = req.params.get('study_name')
    entity_name = req.params.get('entity_name')

    if not study_name or not entity_name:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            study_name = req_body.get('study_name')
            entity_name = req_body.get('entity_name')

    if not study_name or not entity_name:
        logging.error(f"Provided: {study_name} and {entity_name}")
        return func.HttpResponse(
            "Please pass a study_name and entity_name on the query string or in the request body",
            status_code=400)

    url = f"https://clinscience.mdsol.com/RaveWebServices/studies/{study_name}/datasets/regular/{entity_name}.csv"
    logging.info(f"Processing: {url}")
    response = requests.get(url)

    if response.status_code == 200:
        return func.HttpResponse(response.text)
    else:
        return func.HttpResponse(
            f"Failed to retrieve data for study {study_name} and entity {entity_name}",
            status_code=500)


@app.route(route="test", auth_level=func.AuthLevel.ANONYMOUS)
def test(req: func.HttpRequest) -> func.HttpResponse:
    url = "https://your-fixed-url.com"  # replace with your actual URL
    logging.info(f"Testing: {url}")
    response = requests.get(url)

    if response.status_code == 200:
        return func.HttpResponse("Test successful!")
    else:
        return func.HttpResponse(
            f"Test failed with status code {response.status_code}",
            status_code=500)


# @app.route(route="MyHttpTrigger", auth_level=func.AuthLevel.ANONYMOUS)
# def MyHttpTrigger(req: func.HttpRequest) -> func.HttpResponse:
#     logging.info('Python HTTP trigger function processed a request.')

#     name = req.params.get('name')
#     if not name:
#         try:
#             req_body = req.get_json()
#         except ValueError:
#             pass
#         else:
#             name = req_body.get('name')

#     if name:
#         return func.HttpResponse(f"Hello, {name}. This HTTP triggered function executed successfully.")
#     else:
#         return func.HttpResponse(
#              "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.",
#              status_code=200
#         )
